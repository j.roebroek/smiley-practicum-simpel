﻿using System;
using System.Drawing;
using System.Windows.Forms;

class Smile : Form
{
    // The screen and its properties.
    Graphics screen;

    // Dimensions of the screen.
    int sizeX = 800;
    int sizeY = 600;

    #region Other components

    static void Main()
    {
        Application.Run(new Smile());
    }

    Smile()
    {
        // Set the dimensions of the painting canvas.
        this.Size   = new Size(sizeX, sizeY);

        // Give the painting canvas a name.
        this.Text   = "Smile";

        // Call the painting method to paint on the canvas.
        this.Paint += (object o, PaintEventArgs p) => { this.screen = p.Graphics; this.Draw(); };
    }

    /// <summary>
    /// Tekent een rechthoek op locatie (x,y) met breedte <c>width</c> en hoogte <c>height</c>. De kleur kun je kiezen met <c>Color.naam</c>. Bijvoorbeeld: Color.Blue
    /// </summary>
    void DrawRectangle(Color c, int x, int y, int width, int height)
    {
        screen.DrawRectangle(new Pen(c, 5), x, y, width, height);
    }

    /// <summary>
    /// Tekent een ingekleurde rechthoek op locatie (x,y) met breedte <c>width</c> en hoogte <c>height</c>. De kleur kun je kiezen met <c>Color.naam</c>. Bijvoorbeeld: Color.Blue
    /// </summary>
    void FillRectangle(Color c, int x, int y, int width, int height)
    {
        screen.FillRectangle(new SolidBrush(c), x, y, width, height);
    }
    /// <summary>
    /// Tekent een lijn van punt (x1,y1) naar punt (x2,y2). De kleur kun je kiezen met <c>Color.naam</c>. Bijvoorbeeld: Color.Blue
    /// </summary>
    void DrawLine(Color c, int x1, int y1, int x2, int y2)
    {
        screen.DrawLine(new Pen(c, 5), x1, y1, x2, y2);
    }

    /// <summary>
    /// Tekent een cirkel of ovaal met middelpunt (x,y), diameter in de breedte <c>width</c> en diameter in de hoogte <c>height</c>. Een cirkel met diameter 10 heeft dus width=10 en height=10. De kleur kun je kiezen met <c>Color.naam</c>. Bijvoorbeeld: Color.Blue
    /// </summary>
    void DrawCircle(Color c, int x, int y, int width, int height)
    {
        screen.DrawEllipse(new Pen(c, 5), x - width / 2, y - height / 2, width, height);
    }

    /// <summary>
    /// Tekent een ingekleurde cirkel of ovaal met middelpunt (x,y), diameter in de breedte <c>width</c> en diameter in de hoogte <c>height</c>. Een cirkel met diameter 10 heeft dus width=10 en height=10. De kleur kun je kiezen met <c>Color.naam</c>. Bijvoorbeeld: Color.Blue
    /// </summary>
    void FillCircle(Color c, int x, int y, int width, int height)
    {
        screen.FillEllipse(new SolidBrush(c), x - width / 2, y - height / 2, width, height);
    }

    /// <summary>
    /// Tekent een boog tussen punt (x1,y1) en (x2,y2). <c>size</c> geeft aan hoe groot de glimlach is. Probeer hem ook eens negatief te maken! De kleur kun je kiezen met <c>Color.naam</c>. Bijvoorbeeld: Color.Blue
    /// </summary>
    void DrawSmile(Color c, int x1, int y1, int x2, int y2, int size)
    {
        if (size == 0)
            size = 1;
        int dir = size;
        size = Math.Abs(size);
        if (dir == size)
        {
            screen.DrawArc(new Pen(Color.Black, 5), new Rectangle(x1, y1 - size / 2, (x2 - x1), size), 0, 180);
        }
        else
        {
            screen.DrawArc(new Pen(Color.Black, 5), new Rectangle(x1, y1, (x2 - x1), size), 180, 180);
        }

    }

    #endregion

    void Draw()
    {
        //DrawRectangle
        DrawRectangle(Color.Green, 200, 100, 200, 100);

        //FillRectangle
        FillRectangle(Color.Blue, 50, 50, 200, 100);
        
        //DrawLine
        DrawLine(Color.Red, 50, 160, 250, 260);
        
        //DrawCircle
        DrawCircle(Color.Red, 70, 70, 100, 100);

        //FillCircle
        FillCircle(Color.MediumPurple, 450, 100, 100, 100);

        //DrawSmile
        DrawSmile(Color.Aqua, 80, 350, 280, 350, 80);
    }
}